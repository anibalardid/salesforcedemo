#!/bin/bash

phpcbf application/* --extensions=php | grep Patched
phpcbf public/* --extensions=php | grep Patched

find . -name \*.php -exec php -l "{}" \; | grep parsing

