$(document).ready(function () {
    $('.loginusername').keyup(function () {
        let username = $('.loginusername').val();

        // remove spaces from username
        username = username.replace(/[^A-Za-z0-9\.\@\-\_]+/g, '');
        username = username.toLowerCase();
        $('.loginusername').val(username);
    });

    // LoginForm submit
    $('#LoginForm').submit(function(e) {

        e.preventDefault();
        e.stopPropagation();

        $("#loginerror").hide();

        var errors = false;

        $('input.requiredlogin').each(function () {
            if ($.trim($(this).val()) == '') {
                errors = true;
                $(this).addClass('error');
                $(this).css("border", "1px solid #FF0000");
            } else {
                $(this).removeClass('error');
                $(this).css("border", "1px solid #DFDFDF");
            }
        });

        if (!errors) {
            var dataform = $('#LoginForm').serialize();

            $('.submitloginbtn').attr('disabled', 'disabled');

            $.ajax({
                url: URL + 'home/login',
                type: 'POST',
                data: dataform,
                dataType: "json",
                success: function (response) {
                    // console.log("response:", response);
                    errors = false;
                    if (response.error && response.error == "1") {
                        errors = true;
                        $('#loginerror').removeClass('hide');
                        $('#loginerror').show().html('Error: ' + response.errorMsg);
                    }
                    
                    if (!errors) {
                        // redirect to dashboard
                        console.log("redirecting to " + URL_DASHBOARD);
                        window.location.href = URL_DASHBOARD;
                        return;
                    } else {
                        // If there is an error
                        $('.submitloginbtn').removeAttr('disabled');
                        //console.log("response:", response);
                        return;
                    }

                },
                error: function (response) {
                    $('.submitloginbtn').removeAttr('disabled');

                    console.error("error", response);
                    if (response.status == 403 || response.status == 401) {
                        $('#loginerror').removeClass('hide');
                        $('#loginerror').show().html('Error: ' + response.responseJSON.errorMsg);
                    }
                }

            });
        } else {
        }
    }); // end LoginForm submit



});
