<?php
/**
 * MINI - an extremely simple naked PHP application
 *
 * @package mini
 * @author  Panique
 * @link    http://www.php-mini.com
 * @link    https://github.com/panique/mini/
 * @license http://opensource.org/licenses/MIT MIT License
 */

// set a constant that holds the project's folder path, like "/var/www/".
// DIRECTORY_SEPARATOR adds a slash to the end of the path
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
// set a constant that holds the project's "application" folder, like "/var/www/application".
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);

// This is the auto-loader for Composer-dependencies (to load tools into your project).
require ROOT . 'vendor/autoload.php';

// load application config (error reporting etc.)
require APP . 'config/constants.php';

// load application class
use Mini\Core\Application;

/*
|--------------------------------------------------------------------------
| Register Error & Exception handlers
|--------------------------------------------------------------------------
|
| Here we will register the methods that will fire whenever there is an error
| or an exception has been thrown.
|
*/
Mini\Core\Handler::register();

$lang = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) : 'en';

$acceptLang = ['en', 'es']; 
$lang = in_array($lang, $acceptLang) ? $lang : 'en';
DEFINE('DEFAULT_LANG', $lang);

// start the application
$app = new Application();