<?php

 /**
  * This file contains configuration for the application.
  * It will be used by application/Core/Config.php
  */

// load configuration from file
if (file_exists(APP . 'config/data.ini') ) {
    $configuration = parse_ini_file(APP . 'config/data.ini');
} else {
    die('missing configuration file.');
}

return $configuration;
