<?php

/**
 * Configuration
 */

/**
 * Configuration for: Error reporting
 * Useful to show every little problem during development, but only show hard errors in production
 */

// load configuration from file
if (file_exists(APP . 'config/data.ini') ) {
    $configuration = parse_ini_file(APP . 'config/data.ini');
} else {
    die('missing configuration file.');
}
define('ENVIRONMENT', $configuration['ENVIRONMENT']);
define('DEBUGBAR', $configuration['DEBUGBAR']);

if (ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

/**
 * Configuration for: URL
 * Here we auto-detect your applications URL and the potential sub-folder. Works perfectly on most servers and in local
 * development environments (like WAMP, MAMP, etc.). Don't touch this unless you know what you do.
 *
 * URL_PUBLIC_FOLDER:
 * The folder that is visible to public, users will only have access to that folder so nobody can have a look into
 * "/application" or other folder inside your application or call any other .php file than index.php inside "/public".
 *
 * URL_PROTOCOL:
 * The protocol. Don't change unless you know exactly what you do. This defines the protocol part of the URL, in older
 * versions of MINI it was 'http://' for normal HTTP and 'https://' if you have a HTTPS site for sure. Now the
 * protocol-independent '//' is used, which auto-recognized the protocol.
 *
 * URL_DOMAIN:
 * The domain. Don't change unless you know exactly what you do.
 * If your project runs with http and https, change to '//'
 *
 * URL_SUB_FOLDER:
 * The sub-folder. Leave it like it is, even if you don't use a sub-folder (then this will be just "/").
 *
 * URL:
 * The final, auto-detected URL (build via the segments above). If you don't want to use auto-detection,
 * then replace this line with full URL (and sub-folder) and a trailing slash.
 */

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', $configuration['DASHBOARDPROTOCOL']);
define('URL_DOMAIN', (isset($_SERVER['HTTP_HOST']))?$_SERVER['HTTP_HOST']:'localhost');
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);
define('URL_DASHBOARD', URL . 'dashboard');

define('DS', DIRECTORY_SEPARATOR);
define('TEMPLATES_PATH', ROOT . 'view/_templates' . DS);
define('PUBLIC_PATH', ROOT . 'public' . DS);
define('VIEWS_PATH', APP . 'view' . DS);
define('LOGS_PATH', ROOT . 'logs' . DS . 'log-' . ENVIRONMENT . '.log');
define('ERROR_LOGS_PATH', ROOT . 'logs' . DS . 'log-error-' . ENVIRONMENT . '.log');
