
        <!-- Page Content -->
        <div id="page-wrapper" style="min-height: 294px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12" style="text-align:center; font-family:'Trebuchet MS', Helvetica, sans-serif;">
                        <h1 class="page-header" style="font-size: 80px;">400</h1>
                        <h2 class="page-header">Bad Request</h2>
                        <?php if ($data && isset($data['errorMsg']) && $data['errorMsg']){ ?>
                            <p style="border: 1px solid #000; font-weight: bold; margin: 50px; padding: 20px; background: #c4183c; color: #fad7de;"><?php $this->controller->base->translate($data['errorMsg']); ?></p>
                            <p style="margin: 50px; padding: 20px;"><a href="/contact"><?php $this->controller->base->translate('plans-contact-us'); ?></a></p>
                        <?php } ?>
                    </div>
					<hr>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
