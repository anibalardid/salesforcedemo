<div class="mt-5">
    <div class="container">

        <!-- #contacts -->
        <div id="contacts" class="bg-white text-blue-dark text-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <h3 class="mn-3">Contacts List</h3>

                        <div>
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Type</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? foreach($contacts as $contact) { ?>
                                    <tr>
                                        <th scope="row"><?php echo $contact['attributes']['type']; ?></th>
                                        <th><?php echo $contact['Name']; ?></th>
                                        <th><?php echo $contact['Email']; ?></th>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- #contacts -->

    </div>
    <!-- .container -->
</div>

<footer class="text-center mt-4">
    <a href="<?php echo URL; ?>home/logout">Logout</a>
</footer>