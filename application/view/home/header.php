<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Salesforce Demo</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" crossorigin="anonymous">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <?php $this->controller->base->loadjs("landing.js"); ?>

    <script>
        var URL = '<?php echo URL; ?>';
        var URL_DASHBOARD = '<?php echo URL_DASHBOARD; ?>';
    </script>

</head>
<body>
