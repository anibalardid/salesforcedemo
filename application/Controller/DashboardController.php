<?php

namespace Mini\Controller;

use Mini\Core\Controller;
use Mini\Model\Salesforce;

class DashboardController extends Controller
{

    /**
     * Dashboard index
     */
    public function index()
    {
        $this->base->log(__FUNCTION__, $this->request, __FILE__, __LINE__);
        
        // validate if user is logged in
        $this->validateLoggedUser();
        
        $userdata = $this->base->session->getAll();
        $this->base->log("userdata:", $userdata, __FILE__, __LINE__);

        $salesforce = new Salesforce();
        $contacts = $salesforce->getContacts();

        // load views
        $this->view->renderWithLayouts(
            APP . 'view/home/', // header and footer
            APP . 'view/dashboard/index.php', // view
            [
                'contacts' => $contacts
            ]
        );
    }


}
