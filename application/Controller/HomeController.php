<?php

/**
 * Class HomeController
 */

namespace Mini\Controller;

use Mini\Core\Controller;
use Mini\Model\Salesforce;

class HomeController extends Controller
{

    /**
     * Index
     *
     * @return void
     */
    public function index()
    {
        $this->base->log(__FUNCTION__, $this->request, __FILE__, __LINE__);
        $params = $this->request->data;
        $query = $this->request->query;
        
        $userdata = $this->base->session->getAll();

        if (isset($userdata['is_logged_in']) && $userdata['is_logged_in']) {
            $this->redirector->redirect(URL_DASHBOARD);
        }

        $csrf = SHA1(uniqid());
        $this->base->session->csrf = $csrf;

        // load views
        $this->view->renderWithLayouts(
            APP . 'view/home/', // header and footer
            APP . 'view/home/login.php', // view
            [
                'csrf' => $csrf, 
                'config' => $this->base->config
            ]
        );
    }

    /**
     * [login description]
     *
     * @return  [type]  [return description]
     */
    public function login()
    {
        $this->base->log(__FUNCTION__, $this->request, __FILE__, __LINE__);

        $params = $this->request->data;

        // validate if is called by ajax
        $this->validateAjax();

        // validate csrf field of the form
        $this->validateCSRF();

        $username = (isset($params['username'])) ? $params['username'] : null;
        $password = (isset($params['password'])) ? $params['password'] : null;

        // validate if send username or email
        if (!filter_var($username, FILTER_VALIDATE_EMAIL) || !$username || !$password) {
            // error
            die('Error in username or password');
        }

        $salesforce = new Salesforce();
        $result = $salesforce->login($username, $password);

        // $this->base->log(__FUNCTION__, "result:" . print_r($result, true), __FILE__, __LINE__);

        if ($result != 'ok') {
            $this->_response['statusCode'] = '401';
            $this->_response['error'] = '1';
            $this->_response['errorMsg'] = $result;
        } else {
            $this->_response['statusCode'] = '200';
            $this->_response['error'] = '0';
            $this->_response['errorMsg'] = '';
        }

        // render json response
        $this->view->renderJson($this->_response);
    }

    /**
     * logout and redirect to homepage
     */
    public function logout() 
    {
        $this->base->session->clean();

        $this->redirector->redirect(URL);
    }



}
