<?php

/**
 * Errors controller
 *
 * Errors controller can be only accessed from within the application itself,
 * So, any request that has errors as controller will be considered as invalid
 *
 * @see App::isControllerValid()
 */

namespace Mini\Controller;

use Mini\Core\Controller;
use Mini\Model\Actions;

class ErrorsController extends Controller
{

    /**
     * Initialization method.
     */
    public function initialize()
    {
    }

    public function NotFound()
    {
        $this->base->log(__FUNCTION__, $this->request, 'ERROR', '404');
        $data = [
            'statusCode' => '404'
        ];
        
        if (PHP_SAPI === 'cli') { die("Error " . $data['statusCode'] . "\n"); 
        }

        $this->view->renderWithLayouts( 
            VIEWS_PATH . "layout/errors/", 
            VIEWS_PATH . "errors/404.php",
            $data
        );
    }

    public function Unauthenticated()
    {
        $data = [
            'statusCode' => '401'
        ];

        if (PHP_SAPI === 'cli') { die("Error " . $data['statusCode'] . "\n"); 
        }

        $this->view->renderWithLayouts(
            VIEWS_PATH . "layout/errors/",
            VIEWS_PATH . "errors/401.php",
            $data
        );
    }

    public function Unauthorized()
    {
        $data = [
            'statusCode' => '403'
        ];

        if (PHP_SAPI === 'cli') { die("Error " . $data['statusCode'] . "\n"); 
        }

        $this->view->renderWithLayouts(
            VIEWS_PATH . "layout/errors/",
            VIEWS_PATH . "errors/403.php",
            $data
        );
    }

    public function BadRequest()
    {
        $data = [
            'statusCode' => '400'
        ];

        if (PHP_SAPI === 'cli') { die("Error " . $data['statusCode'] . "\n"); 
        }
        
        $this->view->renderWithLayouts(
            VIEWS_PATH . "layout/errors/",
            VIEWS_PATH . "errors/400.php",
            $data
        );
    }

    public function System()
    {
        $data = [
            'statusCode' => '500'
        ];
        
        if (PHP_SAPI === 'cli') { die("Error " . $data['statusCode'] . "\n"); 
        }

        $this->view->renderWithLayouts(
            VIEWS_PATH . "layout/errors/",
            VIEWS_PATH . "errors/500.php",
            $data
        );
    }
}
