<?php

namespace Mini\Model;

use Mini\Core\Model;

class Salesforce extends Model
{

    /**
     * [login description]
     *
     * @param   [type]  $username  [$username description]
     * @param   [type]  $password  [$password description]
     *
     * @return  [type]             [return description]
     */
    public function login($username, $password) 
    {
        $this->base->log(__FUNCTION__, func_get_args(), __FILE__, __LINE__);

        $sfClient = $this->_connect();

        try {
            $accesstoken = $sfClient->login($username, $password);
        } catch (\Napp\Salesforce\Exceptions\RequestException $e) {
            $this->base->log(__FUNCTION__, "e->getMessage():" . print_r($e->getMessage(), true), __FILE__, __LINE__);
            $this->base->log(__FUNCTION__, "e->getErrorCode():" . print_r($e->getErrorCode(), true), __FILE__, __LINE__);
            // echo $e->getMessage();
            return $e->getErrorCode() . ' - ' . $e->getMessage();
        } catch (\Napp\Salesforce\Exceptions\AuthenticationException $e) {
            $this->base->log(__FUNCTION__, "e->getErrorCode():" . print_r($e->getErrorCode(), true), __FILE__, __LINE__);
            return $e->getErrorCode() . ' - ' . $e->getMessage();
        }

        $this->base->log(__FUNCTION__, "accesstoken:" . print_r($accesstoken, true), __FILE__, __LINE__);

        $this->base->session->token = $accesstoken->toJson();
        $this->base->session->username = $username;
        $this->base->session->is_logged_in = true;

        return 'ok';
    }


    /**
     * [getContacts description]
     *
     * @return  [type]  [return description]
     */
    public function getContacts() 
    {
        $this->base->log(__FUNCTION__, func_get_args(), __FILE__, __LINE__);

        $sfClient = $this->_connect();

        $tokenGenerator = new \Napp\Salesforce\AccessTokenGenerator();
        $userdata = $this->base->session->getAll();

        $accessToken = $tokenGenerator->createFromJson($userdata['token']);
        $sfClient->setAccessToken($accessToken);
        
        $results = $sfClient->search('SELECT Name, Email FROM Lead Limit 10');

        $this->base->log(__FUNCTION__, "\nresults:\n" . print_r($results, true), __FILE__, __LINE__);

        return $results;
    }

    /**
     * [_connect description]
     *
     * @return  [type]  [return description]
     */
    protected function _connect() 
    {
        $this->base->log(__FUNCTION__, func_get_args(), __FILE__, __LINE__);

        $loginurl = $this->base->config['LOGIN_URL'];
        $client_id = $this->base->config['CONSUMER_KEY'];
        $client_secret = $this->base->config['CONSUMER_SECRET'];

        $sfClient = \Napp\Salesforce\Client::create($loginurl, $client_id, $client_secret, 'v37.0');

        return $sfClient;
    }

}
