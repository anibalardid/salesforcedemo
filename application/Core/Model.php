<?php

namespace Mini\Core;

use PDO;
use Mini\Core\Base;
use Mini\Core\Config;

class Model
{
    /**
     * @var null Database Connection
     */
    public $db = null;
    public $base = null;

    /**
     * Whenever model is created, open a database connection.
     */
    function __construct()
    {
        try {
            self::openDatabaseConnection();
        } catch (\PDOException $e) {
            exit('Database connection could not be established.');
        }

        $this->base = new Base();
    }

    /**
     * Open the database connection with the credentials from application/config/config.php
     */
    private function openDatabaseConnection()
    {
        if (Config::get('DB_TYPE') == "none") {
            $this->db = null;
            return;
        }

        // set the (optional) options of the PDO connection. in this case, we set the fetch mode to
        // "objects", which means all results will be objects, like this: $result->user_name !
        // For example, fetch mode FETCH_ASSOC would return results like this: $result["user_name] !
        // @see http://www.php.net/manual/en/pdostatement.fetch.php
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        
        // setting the encoding is different when using PostgreSQL
        if (Config::get('DB_TYPE') == "pgsql") {
            $databaseEncodingenc = " options='--client_encoding=" . Config::get('DB_CHARSET') . "'";
        } else {
            $databaseEncodingenc = "; charset=" . Config::get('DB_CHARSET');
        }

        // generate a database connection, using the PDO connector
        // @see http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
        $this->db = new PDO(Config::get('DB_TYPE') . ':host=' . Config::get('DB_HOST') . ';dbname=' . Config::get('DB_NAME') . $databaseEncodingenc, Config::get('DB_USER'), Config::get('DB_PASS'), $options);
    }

}
