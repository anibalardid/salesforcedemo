<?php

namespace Mini\Core;

use Mini\Core\Base;
use Mini\Controller\ErrorsController;

class Controller
{
    public $base = null;
    protected $view = null;
    
    /**
     * request
     */
    public $request = null;
    public $response = null;

    protected $_response = [
        'data' => '',
        'statusCode' => '200',
        'error' => '0',
        'errorMsg' => ''
    ];

    /**
     * Whenever model is created, open a database connection.
     */
    public function __construct(Request $request = null, Response $response = null) 
    {
        $this->base = new Base();

        $this->request = ($request !== null) ? $request  : new Request();
        $this->response = ($response !== null) ? $response : new Response();

        $this->view = new View($this);
        $this->redirector = new Redirector();
    }

    /**
     * show error page
     *
     * call error action method and set response status code
     * This will work as well for ajax call, see how ajax calls are handled in main.js
     *
     * @param int|string $code
     */
    public function error($code)
    {

        $errors = [
            404 => "notfound",
            401 => "unauthenticated",
            403 => "unauthorized",
            400 => "badrequest",
            500 => "system"
        ];
        if(!isset($errors[$code]) || !method_exists("ErrorsController", $errors[$code])) {
            $code = 500;
        }

        $action = isset($errors[$code])? $errors[$code]: "System";
        $this->response->setStatusCode($code);

        // clear, get page, then send headers
        $this->response->clearBuffer();
        (new ErrorsController($this->request, $this->response))->{$action}();
        return $this->response;
    }

    /**
     * Validate if controller is called by ajax
     */
    public function validateAjax() 
    {
        if(!$this->request->isAjax()) { 
            $page = new \Mini\Controller\ErrorsController();
            $page->BadRequest();
            $page->response->send();
        }
    }

    /**
     * [validateLoggedUser description]
     * 
     * @return [type] [description]
     */
    public function validateLoggedUser() 
    {
        $userdata = $this->base->session->getAll();

        if (!isset($userdata['is_logged_in']) || !$userdata['is_logged_in'] || !isset($userdata['username']) || !$userdata['username'] || !isset($userdata['token'])  || !$userdata['token']) 
        {
            if(!$this->request->isAjax()) { 
                $this->redirector->redirect(URL . 'home/logout');
            } else {
                die('logged out');
            }
        }
        
    }

    /**
     * [validateFreeAccount description]
     * 
     * @return [type] [description]
     */
    public function validateFreeAccount() 
    {
        $userdata = $this->base->session->getAll();

        $userplan = $userdata['plan'];

        if ($userplan == 'free') {
            if(!$this->request->isAjax()) { 
                $this->redirector->redirect(URL . 'user/logout');
            } else {
                $error = $this->base->translate('dashboard-error-no-access', false);
                
                $response = [
                    'statusCode' => '200',
                    'error' => '1',
                    'errorMsg' => $error
                ];

                die(json_encode($response));
            }
        }
    }

    /**
     * [validateAdminUser description]
     * 
     * @return [type] [description]
     */
    public function validateAdminUser() 
    {
        $userdata = $this->base->session->getAll();

        if (!isset($userdata['plan']) || $userdata['plan'] != 'admin') {
            if(!$this->request->isAjax()) { 
                $this->redirector->redirect(URL . 'user/logout');
            } else {
                die('logged out');
            }
        }

        return true;
    }
        

    /**
     * Validate CSRF field
     */
    public function validateCSRF() 
    {
        $csrfok = (isset($this->base->session->csrf) && $this->base->session->csrf) ? $this->base->session->csrf : null;
        $csrf = (isset($this->base->post_params['csrf'])) ? $this->base->post_params['csrf'] : null;
        
        if (!$csrfok || !$csrf || $csrfok != $csrf) {
            $this->_response['statusCode'] = '403';
            $this->_response['errorMsg'] = $this->base->translate('login-bad-csrf', false);
            $this->_response['error'] = 1;
            $this->_response['data'] = null;
            
            $this->view->renderJson($this->_response);
            die();
        }

    }

    /**
     * Check if CSRF is valid
     */
    public function isValidCSRF() 
    {
        $csrfok = (isset($this->base->session->csrf) && $this->base->session->csrf) ? $this->base->session->csrf : null;
        $csrf = (isset($this->base->post_params['csrf'])) ? $this->base->post_params['csrf'] : null;
        
        if (!$csrfok || !$csrf || $csrfok != $csrf) {
            return false;
        }
        
        return true;
    }

    public function getDebugHeader() 
    {
        if ((ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') && DEBUGBAR) {
            list($cssFiles, $jsFiles) = $this->base->debugbarRenderer->getAssets();
            foreach ($cssFiles as $css) {
                $css = str_replace(ROOT . 'vendor/maximebf/debugbar/src/DebugBar/Resources/', URL . 'assets/debugbar/', $css);
                if (strpos($css, 'awesome') == false) {
                    echo '<link rel="stylesheet" href="'.$css.'">'."\n";
                }
            }
            foreach ($jsFiles as $js) {
                $js = str_replace(ROOT . 'vendor/maximebf/debugbar/src/DebugBar/Resources/', URL . 'assets/debugbar/', $js);
                if (strpos($js, 'jquery') == false) {
                    echo '<script src="'.$js.'"></script>'."\n";
                }
            }
        }
    }

    public function getDebugFooter() 
    {
        if ((ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') && DEBUGBAR) {
            echo $this->base->debugbarRenderer->render();
        }
    }
        

}
