<?php

namespace Mini\Core;

class Session
{
    const SESSION_STARTED = true;
    const SESSION_NOT_STARTED = false;
   
    // The state of the session
    private $sessionState = self::SESSION_NOT_STARTED;
    private $sessionID = null;
   
    // THE only instance of the class
    private static $instance;
   
   
    private function __construct() 
    {
    }
   
   
    /**
    *    Returns THE instance of 'Session'.
    *    The session is automatically initialized if it wasn't.
    *   
    *    @return object
    **/
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
       
        self::$instance->startSession();
       
        return self::$instance;
    }
   
   
    /**
    *    (Re)starts the session.
    *   
    *    @return bool    TRUE if the session has been initialized, else FALSE.
    **/
    public function startSession()
    {
        if ($this->sessionState == self::SESSION_NOT_STARTED) {
            $this->sessionState = session_start();
        }

        if (isset($_COOKIE['PHPSESSID'])) {
            //session_id($_COOKIE['PHPSESSID']);
            $this->sessionID = $_COOKIE['PHPSESSID'];
        } else {
            $this->sessionID = session_id();
        }

        return $this->sessionState;
    }
   
   
    /**
    *    Stores datas in the session.
    *    Example: $instance->foo = 'bar';
    *   
    *    @param  name    Name of the datas.
    *    @param  value    Your datas.
    *    @return void
    **/
    public function __set( $name , $value )
    {
        $_SESSION[$name] = $value;
    }
   
   
    /**
    *    Gets datas from the session.
    *    Example: echo $instance->foo;
    *   
    *    @param  name    Name of the datas to get.
    *    @return mixed    Datas stored in session.
    **/
    public function __get( $name )
    {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
    }
   
    /**
     * [getAll description]
     * 
     * @return [type] [description]
     */
    public function getAll()
    {
        return $_SESSION;
    }
   
    /**
     * [__isset description]
     * 
     * @param  [type] $name [description]
     * @return boolean       [description]
     */
    public function __isset( $name )
    {
        return isset($_SESSION[$name]);
    }
   
    /**
     * [__unset description]
     * 
     * @param [type] $name [description]
     */
    public function __unset( $name )
    {
        unset($_SESSION[$name]);
    }
   
    /**
    *    Destroys the current session.
    *   
    *    @return bool    TRUE is session has been deleted, else FALSE.
    **/
    public function destroy()
    {
        if ($this->sessionState == self::SESSION_STARTED ) {
            $this->sessionState = !session_destroy();
            unset($_SESSION);
           
            return !$this->sessionState;
        }
       
        return false;
    }

    /**
     * [getSessionID description]
     * 
     * @return [type] [description]
     */
    public function getSessionID()
    {
        if ($this->sessionID ) {
            return $this->sessionID;
        }
       
        return false;
    }
   
    /**
    *    Clean the current session.
    **/
    public function clean()
    {
        $_SESSION = [];
    }

    /**
     * Get User ID.
     *
     * @access public
     * @static static method
     * @return string|null
     */
    public static function getUserId()
    {
        return empty($_SESSION["user_id"]) ? null : (int)$_SESSION["user_id"];
    }

    /**
     * Get User Role
     *
     * @access public
     * @static static method
     * @return string|null
     */
    public static function getUserPlan()
    {
        return empty($_SESSION["plan"]) ? null : $_SESSION["plan"];
    }

}