<?php

/**
* Config class.
* Gets a configuration value
*/

namespace Mini\Core;

class Config
{

    /**
     * Array of configurations
     *
     * @var array
     */
    private static $config = [];

    /**
     * Prefixes used to load specific configurations.
     *
     * @var array
     */
    private static $prefix = [
        'default'   => 'variables'
    ];

    /**
     * Get default configuration value(s)
     *
     * @param  $key string
     * @return string|array|null
     */
    public static function get($key)
    {
        return self::_get($key, self::$prefix['default']);
    }

    /**
     * Set or add a default configuration value
     *
     * @param $key string
     */
    public static function set($key, $value)
    {
        self::_set($key, $value, self::$prefix['default']);
    }

    /**
     * Get a configuration value(s)
     *
     * @param  $key string
     * @param  $source string
     * @return string|null
     * @throws Exception if configuration file doesn't exist
     */
    private static function _get($key, $source)
    {

        if (!isset(self::$config[$source])) {

            $config_file = APP . 'config/' . $source . '.php';

            if (!file_exists($config_file)) {
                throw new Exception("Configuration file " . $source . " doesn't exist");
            }

            self::$config[$source] = include $config_file . "";
        }

        if(empty($key)) {
            return self::$config[$source];
        } else if(isset(self::$config[$source][$key])) {
            return self::$config[$source][$key];
        }

        return null;
    }

    /**
     * Set or adds a configuration value
     *
     * @param $key string
     * @param $value string
     * @param $source string
     */
    private static function _set($key, $value, $source)
    {

        // load configurations if not already loaded
        if (!isset(self::$config[$source])) {
            self::_get($key, $source);
        }

        if($key && $source) {
            self::$config[$source][$key] = $value;
        }
    }

    public static function getConfig() 
    {
        $source = self::$prefix['default'];

        if (!isset(self::$config[$source])) {

            $config_file = APP . 'config/' . $source . '.php';

            if (!file_exists($config_file)) {
                throw new Exception("Configuration file " . $source . " doesn't exist");
            }

            self::$config[$source] = include $config_file . "";
        }

        return self::$config[$source];
    }
}
