<?php

namespace Mini\Core;
use Mini\Core\Config;
use DebugBar\StandardDebugBar;


class Base
{
    public $url_controller = null;
    public $url_action = null;
    public $url_params = null;
    public $post_params = null;
    public $session = null;
    public $config = null;
    public $debugbar = null;
    public $debugbarRenderer = null;
    public $logprefix = null;

    public function __construct() 
    {
        $this->_splitUrl();
        $this->config = Config::getConfig();
        if (PHP_SAPI === 'cli') { 
            $this->session = null;
            $this->debugbar = null;
        } else {
            $this->session = Session::getInstance();

            // debugbar
            // http://phpdebugbar.com/docs/readme.html#php-debug-bar
            if ((ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') && DEBUGBAR) {
                $this->debugbar = new StandardDebugBar();
                $this->debugbarRenderer = $this->debugbar->getJavascriptRenderer();
                $this->debugbar["messages"]->info("url_controller: " . print_r($this->url_controller, true));
                $this->debugbar["messages"]->info("url_action: " . print_r($this->url_action, true));
            }
        }

    }

    /**
     * Setea un prefijo para el archivo de logs
     *
     * @param  [type] $prefix
     * @return void
     */
    public function setlogprefix($prefix) 
    {
        $this->logprefix = $prefix;
    }

    public function loadcss($filename) 
    {
        $path = PUBLIC_PATH . 'assets/css/';
        if (file_exists($path . $filename)) {
            $mtime = filemtime($path . $filename);
    
            $url = URL . 'assets/css/' . $filename . '?t=' . $mtime;
    
            echo "<link rel=\"stylesheet\" href=\"$url\">\r\n";
        }
    }

    public function loadjs($filename) 
    {
        $path = 'assets/js/';
        if (file_exists($path . $filename)) {
            $mtime = filemtime($path . $filename);
    
            $url = URL . 'assets/js/' . $filename . '?t=' . $mtime;
    
            echo "<script src=\"$url\"></script>\r\n";
        }
    }

    /**
     * Get and split the URL
     */
    private function _splitUrl()
    {
        if (isset($_GET['url'])) {

            // split URL
            $url = trim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            // By the way, the syntax here is just a short form of if/else, called "Ternary Operators"
            // @see http://davidwalsh.name/php-shorthand-if-else-ternary-operators
            $this->url_controller = isset($url[0]) ? $url[0] : null;
            $this->url_action = isset($url[1]) ? $url[1] : null;

            // Remove controller and action from the split URL
            unset($url[0], $url[1]);

            // Rebase array keys and store the URL params
            $this->url_params = array_values($url);

            // for debugging. uncomment this if you have problems with the URL
            //echo 'Controller: ' . $this->url_controller . '<br>';
            //echo 'Action: ' . $this->url_action . '<br>';
            //echo 'Parameters: ' . print_r($this->url_params, true) . '<br>';
        }
        
        if (isset($_POST) && is_array($_POST) && count($_POST)>0) {
            $post = [];
            
            foreach ($_POST as $key => $value) {
                $value = (!is_array($value) && !is_object($value)) ? filter_var(trim($value), FILTER_SANITIZE_STRING) : $value;
                $post[$key] = $value;
            }

            $this->post_params = $post;
        }
    }

    /**
     * log
     *
     * @access public
     * @param  string $header
     * @param  string $message
     * @param  string $filename
     * @param  string $linenum
     */
    public function log($header="", $message="", $filename="", $linenum="", $trace = false)
    {
        $logfile = LOGS_PATH;
        
        if ($this->logprefix) {
            $logfile = str_replace('.log', '-' . $this->logprefix . '.log', $logfile);
        }

        $date = date("d/m/Y G:i:s");
        $sessionid = (PHP_SAPI === 'cli') ? 'cli' : session_id();

        $err = "[" . $date."] ".$sessionid." - ".$filename."\n".$linenum." - ".$header. "\n";

        $message = (is_array($message) || is_object($message))? print_r($message, true): $message;
        $err .= $message;

        if ($trace) {
            $err .= "\nTrace : " . print_r(debug_backtrace(), true);
        }

        $err .= "\n*******************************************************************\n\n";

        // log/write error to log file
        error_log($err, 3, $logfile);
    }

    /**
     * errorlog
     *
     * @access public
     * @param  string $header
     * @param  string $message
     * @param  string $filename
     * @param  string $linenum
     */
    public function errorlog($header="", $message="", $filename="", $linenum="", $trace = false)
    {

        $logfile = ERROR_LOGS_PATH;
        $date = date("d/m/Y G:i:s");
        $sessionid = (PHP_SAPI === 'cli') ? 'cli' : session_id();

        $err = "[" . $date."] ".$sessionid." - ".$filename."\n".$linenum." - ".$header. "\n";

        $message = (is_array($message) || is_object($message))? print_r($message, true): $message;
        $err .= $message;

        if ($trace) {
            $err .= "\nTrace : " . print_r(debug_backtrace(), true);
        }

        $err .= "\n*******************************************************************\n\n";

        // log/write error to log file
        error_log($err, 3, $logfile);
    }

    public function debugSQL($sql, $parameters = null) 
    {
        $logfile = LOGS_PATH;
        $date = date("d/m/Y G:i:s");
        $sessionid = (PHP_SAPI === 'cli') ? 'cli' : session_id();

        $err = $date." | ".$sessionid. "\n";

        $parameters = print_r($parameters, true);
        $sql = print_r($sql, true);

        $err .= "\nsql: $sql \nvalues: $parameters ";
        $err .= "\n*******************************************************************\n\n";

        error_log($err, 3, LOGS_PATH);
    }

    public function jsonResponse($response) 
    {
        return json_encode($response);
    }

    public function translate($phrase=null, $print=true) 
    {
        if (!$phrase) {
            return;
        }
        
        $lang = ($this->session->lang) ? $this->session->lang : DEFAULT_LANG;
        //$lang = (isset($_SESSION['lang']) && $_SESSION['lang']) ? $_SESSION['lang'] : DEFAULT_LANG;

        $translations = json_decode(file_get_contents(APP . 'translations/' . $lang . '.json'), true);

        if ($print) {
            echo (isset($translations[$phrase])) ? $translations[$phrase] : $phrase;
        } else {
            return (isset($translations[$phrase])) ? $translations[$phrase] : $phrase;
        }
    }


}
