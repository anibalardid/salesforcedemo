# Salesforce demo Login

## Setup Local Environment

### Configuration

Edit and configure:   
```bash
vim application/config/data.ini
```

### First Time Installation 

```bash
git clone ...
docker-compose up -d
docker-compose exec app composer install -vvv
cp application/config/data.ini.dev application/config/data.ini
```

Browse to `http://localhost`

## Start Environment

```bash
docker-compose up
```

## Stop Environment

```bash
docker-compose stop
```
